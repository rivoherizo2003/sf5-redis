<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\RequestStack;

class SessionDemoService
{
    protected RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $username
     */
    public function createSessionHello(string $username)
    {
        $today = new \DateTime();
        $session = $this->requestStack->getSession();
        if (null === $session->get('hello')){
            $session->set('hello', $username ." ". $today->format("d/m/Y h:s:i"));
        }
    }
}
<?php

namespace App\Controller\Admin;

use App\Form\PubSubType;
use App\Service\MessageService;
use App\Service\RedisService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 */
#[Route('/admin/post'), IsGranted('ROLE_ADMIN')]
class MessageController extends AbstractController
{
    #[Route('/redis/list', name: 'admin_redis_list')]
    public function index(): Response
    {
        $formPubSub = $this->createForm(PubSubType::class);

        return $this->render('admin/message/index.html.twig', [
            'controller_name' => 'PubSubController',
            'formPubSub' => $formPubSub->createView()
        ]);
    }

    #[Route('/send/new/message', name: 'admin_send_new_msg', methods: ['POST'])]
    public function sendMessage(Request $request, MessageService $pubSubService, RedisService $redisService): JsonResponse{
        return new JsonResponse([
            'status' => $pubSubService->sendMsg($request->request->all()['pub_sub'], $redisService, $this->getUser())
        ]);
    }

    #[Route('/subscribe/channel/message', name: 'admin_subscribe_channel', methods: ['GET'])]
    public function subscribe(Request $request, MessageService $messageService, RedisService $redisService): JsonResponse{
        $msg = $messageService->retrieveMsg($redisService);

        return new JsonResponse([
            'messages' => $msg
        ]);
    }
}
